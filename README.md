# Confluence - Soy-Actions - RTE - LESS


## Action rendering soy template

Since Atlassian makes more and more use of [Soy](https://developers.google.com/closure/templates/) templates, I was wondering, how to render soy templates for an action result.

All Soy templates must be configured as a resource:

	<web-resource key="soy-templates" name="Embedded RTE resources">
		<dependency>com.atlassian.auiplugin:aui-experimental-soy-templates</dependency>
	
		<transformation extension="js">
			<transformer key="jsI18n" />
		</transformation>
		<transformation extension="soy">
			<transformer key="soyTransformer">
				<functions>com.atlassian.confluence.plugins.soy:soy-core-functions
				</functions>
			</transformer>
		</transformation>
	
		<resource name="action-template.js" type="download" location="soy/action-template.soy" />
		<context>page</context>
	</web-resource>

Then register the action and set the soy template location, e.g.:

	<!-- /plugins/embedded-rte/soy.action -->
	<action name="soy"
		class="com.example.plugin.confluence.embeddedrte.SampleAction">
		<result name="success" type="soy">
			<!-- short plugin key / soy template namespace and name -->
			<param name="location">:soy-templates/example.templates.rte.show</param>
		</result>
	</action>

Example page in this plugin: [http://localhost:1990/confluence/plugins/embedded-rte/soy.action](http://localhost:1990/confluence/plugins/embedded-rte/soy.action)


## LESS to CSS maven plugin

I recently stumbled on this neat helpful maven plugin, working well in Atlassian environment, comiling LESS files to CSS:

	<plugin>
		<groupId>org.lesscss</groupId>
		<artifactId>lesscss-maven-plugin</artifactId>
		<version>1.3.3</version>
	    <configuration>
	        <sourceDirectory>${basedir}/src/main/resources/css</sourceDirectory>
	        <outputDirectory>${basedir}/target/classes/css</outputDirectory>
	        <compress>true</compress>
	        <includes>
	            <include>*.less</include>
	        </includes>
	    </configuration>
	    <executions>
	      <execution>
	        <goals>
	          <goal>compile</goal>
	        </goals>
	      </execution>
	    </executions>
	</plugin>


## RTE in velocity for another content type

And finally the main part of this example plugin: I wanted to reuse the RTE (rich text editor), tinymce, which Confluence is using when creating and editing content.
I want to be able to:

 - embed the editor anywhere I want
 - use the resulting markup anyway I like (e.g. creating a [CustomContent](https://docs.atlassian.com/confluence/latest/com/atlassian/confluence/content/CustomContentEntityObject.html))

This is actually not as easy as it may sound. This editor is heavily entwined into Confluence internals and after several days of trial and error, I have the editor working, but with a few drawbacks:

 - The editor can only be used **once** in a page
 - There are many adjustments to do in JS code to make it useful
 
Example page in this plugin: [http://localhost:1990/confluence/plugins/embedded-rte/velocity.action](http://localhost:1990/confluence/plugins/embedded-rte/velocity.action)


**I am still working on this stuff, so please don't mind the comments, wrong named parameters, classes and so on.** 


# Sources

* [https://answers.atlassian.com/questions/222233/how-to-use-soy-in-xwork-actions](https://answers.atlassian.com/questions/222233/how-to-use-soy-in-xwork-actions)
* [https://github.com/marceloverdijk/lesscss-maven-plugin](https://github.com/marceloverdijk/lesscss-maven-plugin)