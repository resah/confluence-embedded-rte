package com.example.plugin.confluence.embeddedrte;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;

@Path("/customContents")
// @Consumes(MediaType.APPLICATION_JSON)
// @Produces(MediaType.APPLICATION_JSON)
public class CustomContentResource {

	private final Logger log = LoggerFactory.getLogger(getClass());

	// var url = Confluence.getContextPath() + "/rest/tinymce/1/content/" +
	// contentId + "/comment";
	// var url = Confluence.getContextPath() + "/rest/tinymce/1/content/" +
	// contentId + "/comments/" + parentCommentId + "/comment";

	@POST
	@Path("/{id}/customContent/")
	@AnonymousAllowed
	@RequiresXsrfCheck
	public Response add(
			@PathParam("id") final Long contentId,
			@FormParam("html") final String html,
			@FormParam("watch") final boolean watch,
			@QueryParam("actions") @DefaultValue("false") final boolean actions,
			@Context final HttpServletRequest req) {

		log.info("CREATE CONTENT");

		// CreateCommentCommand command = commentService
		// .newCreateCommentFromEditorCommand(contentId, html);
		// return createAndRender(command, watch, actions, req);
		return Response.ok().build();
	}

	/**
	 * Create a reply to to the identified comment.
	 * 
	 * @param contentId
	 *            the id of the content to comment on
	 * @param parentCommentId
	 *            the id of the comment to be replied to
	 * @param html
	 *            the editor formatted html.
	 * @param actions
	 *            if true then include the comment actions in the returned
	 *            comment
	 * @return a {@link CommentResult} (or subclass depending on actions
	 *         parameter) in the response.
	 */
	@POST
	@Path("/{id}/{parentId}/customContent")
	@AnonymousAllowed
	@RequiresXsrfCheck
	public Response add(
			@PathParam("id") final Long contentId,
			@PathParam("parentId") final Long parentCommentId,
			@FormParam("html") final String html,
			@FormParam("watch") final boolean watch,
			@QueryParam("actions") @DefaultValue("false") final boolean actions,
			@Context final HttpServletRequest req) {

		log.info("CREATE CONTENT");

		// CreateCommentCommand command = commentService
		// .newCreateCommentFromEditorCommand(contentId, parentCommentId,
		// html);
		// return createAndRender(command, watch, actions, req);
		return Response.ok().build();
	}

}
