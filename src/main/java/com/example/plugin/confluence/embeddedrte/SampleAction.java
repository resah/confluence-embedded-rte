package com.example.plugin.confluence.embeddedrte;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.xwork.ActionViewData;
import com.atlassian.confluence.xwork.ActionViewDataMappings;

public class SampleAction extends ConfluenceActionSupport {

	private static final long serialVersionUID = -881154763788187481L;

	private final Logger log = LoggerFactory.getLogger(getClass());

	// soy parameters
	private final Map<String, Object> soyContext = new HashMap<String, Object>();
	private final Locale locale = new Locale("en", "GB");

	@Override
	public String execute() throws Exception {

		log.info("execute()");

		soyContext.put("something", "strange in the neighborhood");

		return SUCCESS;
	}

	@ActionViewDataMappings
	public Map<String, Object> getData() {
		return soyContext;
	}

	@Override
	@ActionViewData
	public Locale getLocale() {
		return locale;
	}

}
