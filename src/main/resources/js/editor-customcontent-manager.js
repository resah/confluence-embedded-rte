AJS.bind("init.rte", function() {

    /**
     * A Manager that handles comment operations, as instigated by the Editor. This caveat basically means that
     * this manager deals with Editor formatted content. This Manager will deal with both handling the server side
     * operations involved in working with comments as well as also handling the displaying of comment operations by
     * delegating to the Confluence.CommentDisplayManager.
     */
    Confluence.Editor.CustomContentManager = (function($) {
        
        function getAddNewCommentUrl(contentId, actions) {
//            var url = Confluence.getContextPath() + "/rest/tinymce/1/content/" + contentId + "/comment";
        	// rest/customcontent/1.0/customContents/0/customContent
            var url = Confluence.getContextPath() + "/rest/customcontent/1.0/customContents/" + contentId + "/customContent";
            if (actions) {
                url += "?actions=true";
            }
            
            return url;
        };
        
        function getReplyToCommentUrl(contentId, parentCommentId, actions) {
            var url = Confluence.getContextPath() + "/rest/tinymce/1/content/" + contentId + "/comments/" + parentCommentId + "/comment";
            if (actions) {
                url += "?actions=true";
            }
            
            return url;
        };
        
        return {
            
            /**
             * Save a new comment and display it if successful.
             * 
             * @param contentId the id of the content being commented on
             * @param parentId the of the comment being replied to. This should be 0 if this is not a reply
             * @param editorHtml editor formatted HTML which is the body of the comment
             * @param watch if true then start watching the content that is being commented on.
             * @param captcha the supplied captcha value (may be null if there is none supplied).
             * @param highlight true if you want the comment to appear highlighted
             * @param commenter the person making the comment. This is an object with the following structure:
             * {
             *     userName: (string),
             *     displayName: (string),
             *     profilePicture: {
             *         isDefault: (boolean),
             *         path: (string)
             *     }
             * }
             * @param successCallback a function taking a single parameter which represents the server returned comment that 
             * is called on success. The parameter has the structure:
             * {
             *    id: (number) the id of the comment
             *    html: (HTML string) the rendered content of the comment
             *    ownerId: (number) the id of the content commented upon
             *    parentId: (number) the id of the comment this one is in reply to
             * }
             * @param errorCallback a function taking a single parameters which describes the error returned
             */
            addComment: function(contentId, parentId, editorHtml, watch, captcha, highlight, commenter, successCallback, errorCallback) {
                var saveCommentSuccessHandler = function(data) {
                    Confluence.CommentDisplayManager.addComment(commenter, data, highlight);                
                    successCallback(data);
                };
                
                Confluence.Editor.CustomContentManager.saveContent(contentId, parentId, editorHtml, saveCommentSuccessHandler, errorCallback);                
            },
            
            /**
             * Save a new comment. If you also want to display the saved comment you should call addComment.
             * 
             * @param contentId the id of the content being commented on
             * @param parentId the of the comment being replied to. This should be 0 if this is not a reply
             * @param editorHtml editor formatted HTML which is the body of the comment
             * @param watch if true then start watching the content that is being commented on.
             * @param captcha the supplied captcha object
             * @param successCallback a function taking a single parameter which represents the server returned comment that 
             * is called on success. The parameter has the structure:
             * {
             *    id: (number) the id of the comment
             *    html: (HTML string) the rendered content of the comment
             *    ownerId: (number) the id of the content commented upon
             *    parentId: (number) the id of the comment this one is in reply to
             * }
             * @param errorCallback a function taking a single parameters which describes the error returned
             */            
            saveContent: function(contentId, parentId, editorHtml, watch, captcha, successCallback, errorCallback) {
                var url = null;
                if (parentId) {
                    url = getReplyToCommentUrl(contentId, parentId, true);
                } else {
                    url = getAddNewCommentUrl(contentId, true);
                }                
                
                var saveCommentSuccessHandler = function(data, textStatus, jqXHR) {
                    successCallback(data);
                };                
                
                var saveCommentErrorHandler = function(jqXHR, textStatus, errorThrown) {
                    var message = textStatus + ": " + errorThrown;
                    if (jqXHR.responseText) {
                        message = message + " - " + jqXHR.responseText;
                    }
                    errorCallback(message);
                };       
                
                var ajaxData = {
                    type: "POST",
                    url: url,
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    data: {
                        "html": editorHtml,
                        "watch": watch
                    },
                    dataType : "json",
                    cache: false,
                    headers: {
                        "X-Atlassian-Token" : "nocheck"  
                    },
                    success : saveCommentSuccessHandler,
                    error : saveCommentErrorHandler,
                    timeout: 120000
                }; 
                
                if (captcha && captcha.id) {
                    ajaxData.headers["X-Atlassian-Captcha-Id"] = captcha.id;
                    ajaxData.headers["X-Atlassian-Captcha-Response"] = captcha.response;
                }
                
                AJS.$.ajax(ajaxData);  
            }
        };    
    })(AJS.$);
});