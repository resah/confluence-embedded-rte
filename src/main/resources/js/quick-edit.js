(function($){

/**
 * Depends on AJS.Confluence.EditorLoader.
 */
$(function(){
    // Pre-load the editor (is there actually any point in having this delayed/in a different event loop?)
    setTimeout(function() {

        // Useful for development if you have batching turned off and aren't testing Comments.
        if (AJS.DarkFeatures.isEnabled('disable-quick-edit')) {
            AJS.log("disable-quick-edit is turned on; run AJS.Confluence.EditorLoader.load() manually.");
            return;
        }

        AJS.log("QuickComment: instigated background loading of the comment editor.");
        AJS.Confluence.EditorLoader.load();
    }, 1000);
    $.each(enableQueue, Function.call);
    ready = true;

});

//Common code used by handlers
var handlers = [],
    ready = false,
    enableQueue = [],
    activated = false;

AJS.Confluence.QuickEdit = {
    register: function(handler){
        handlers.push(handler);
        if(ready){
            handler.enable();
        } else {
            enableQueue.push(handler.enable);
        }
    },
    disableHandlers: function(){
        $.each(handlers, function(i, current){
            return current.disable();
        })
    },
    enableHandlers: function(){
        $.each(handlers, function(i, current){
            return current.enable();
        })
    },
    /**
     * An object that binds actions to the save bar as necessary
     */
    SaveBarBinder: {
        _saveHandler: null,
        _cancelHandler: null,

        bind: function(saveHandler, cancelHandler) {
            if (!AJS.Confluence.QuickEdit.SaveBarBinder.hasBound()) {
                AJS.Confluence.QuickEdit.SaveBarBinder._saveHandler = saveHandler;
                Confluence.Editor.addSaveHandler(saveHandler);
                AJS.Confluence.QuickEdit.SaveBarBinder._cancelHandler = cancelHandler;
                Confluence.Editor.addCancelHandler(cancelHandler);
            }
        },

        /**
         * @returns true if we have already bound save or cancel handlers. Otherwise false.
         */
        hasBound: function() {
            return AJS.Confluence.QuickEdit.SaveBarBinder._saveHandler != null || AJS.Confluence.QuickEdit.SaveBarBinder._cancelHandler != null;
        }
    },

    /**
     * Activate the dynamic editor on the supplied container.
     *
     * @param $container the container containing the necessary structure to activate the
     * editor within.
     * @param saveHandler the function to be called when save is activated on the editor
     * @param cancelHandler the function to be called when the editor is cancelled.
     * @param transition the function to be called to handle the UI involved in transitioning from placeholder
     * to editor. This function should take the parameters $form, $loadingContainer, $spacer.
     * @param fallbackUrl the url presented to the user if there is an error activating the editor
     */
    activateEditor: function($container, saveHandler, cancelHandler, transition, fallbackUrl) {
        var $form = $(".quick-customcontent-form", $container);
        var $loadingContainer = $(".quick-customcontent-loading-container", $container);
        var $spacer = $(".quick-customcontent-vertical-spacer", $container);

        // Prevent multiple activation (e.g. due to popstate events fired by Chrome)
        if(activated) {
            return;
        }

        activated = true;

        if(!$form.length) {
            // bad page - cannot quick edit / comment, redirect to fallbackUrl
            AJS.log('Editor form cannot be found - defaulting to normal edit');
            AJS.trigger('analyticsEvent', {name: 'quick-edit-failure' });
            window.location = fallbackUrl;
            return;
        }

        var cancelLoadingMessage;

        var unblocker = AJS.Confluence.BlockAndBuffer.block($(document));

        /**
         * Display the given error message inside the provided container.
         */
        var displayError = function(message, fallbackUrl) {
            $loadingContainer.hide();
            $(".quick-customcontent-body", $container).append("<div class=\"quick-customcontent-error-box\"></div>");
            AJS.messages.error(".quick-customcontent-error-box", {
                title: AJS.I18n.getText('quick.comment.loading.error.message'),
                body: Confluence.Templates.Comments.editorLoadErrorMessage({ message: message, fallbackUrl: fallbackUrl}),
                closeable: false
            });
        };

        // If loading fails
        var errorTimeoutId = setTimeout(function() {
            cancelLoadingMessage(false);
            unblocker();
            displayError(AJS.I18n.getText('quick.comment.loading.error.timeout'), fallbackUrl);
        }, AJS.Confluence.EditorLoader.loadingTimeout + 1000);

        var cancelTimeoutMessage = function() {
            clearTimeout(errorTimeoutId);
        };

        var setEditorText = function(text) {
            var editor = AJS.Rte.getEditor();
            text = text || "";
            var body = $(AJS.Rte.getEditor().getBody());
            var firstExistingLine = body.children(':first').text();
            var restHtml = $('<div></div>').append(body.children().not(':first')).html();
            var lines = text.split('\r');
            var html = '';
            var i, l, line;
            for(i = 0, l = lines.length; i < l; i++) {
                line = lines[i];
                if(i == lines.length - 1)
                    line += firstExistingLine;
                if(line)
                    html += '<p>' + $('<p></p>').text(line).html() + '</p>';
                else
                    html += '<p><br></p>';
            }
            html += restHtml;

            // todo: Do we need this? Quick testing suggests it's not used.  page.js actually does this.
            editor.setContent(html, {format : 'html'});

            // ensure cursor is placed after any text that is added
            var selNode = body.find('p').last()[0];
            var sel = editor.selection;
            sel.select(selNode, true);
            sel.collapse(false);
            sel.normalize();
        }

        var successCallback = function() {
            cancelLoadingMessage(true);
            cancelTimeoutMessage();

            $(".quick-customcontent-prompt", $container).hide();
            $loadingContainer.hide();
            $(".quick-customcontent-body", $container).addClass("customcontent-body");
            $form.fadeIn("fast", function() {
                $spacer.hide();
                AJS.Confluence.QuickEdit.SaveBarBinder.bind(saveHandler, cancelHandler);

                setEditorText(unblocker());

                // Duplicated from EditorLoader but Firefox requires the RTE focus
                // to be set after the RTE has become visible
                AJS.Rte.getEditor().focus();
            });
            AJS.trigger('quickedit.success');
        };

        var errorCallback = function(message) {
            cancelLoadingMessage(false);
            cancelTimeoutMessage(); // we have more specific error. Don't show this one.
            unblocker();
            displayError(message, fallbackUrl);
        };

        var editorActivated = AJS.Confluence.EditorLoader.activate($form);
        editorActivated.progress(function(){
            cancelLoadingMessage = transition($form, $loadingContainer, $spacer);
        });
        editorActivated.always(function(){
            if (!cancelLoadingMessage) {
                cancelLoadingMessage = transition($form, $loadingContainer, $spacer);
            }
        });
        editorActivated.done(successCallback);
        editorActivated.fail(errorCallback);

        // Fix for CONFDEV-17765: We need to take all aui-messages with the 'closeable' class and make them
        // closeable manually. AUI runs the same method on DOM ready, but with quick-edit this content is not
        // in the DOM yet at this point in time because it's fetched async.
        editorActivated.done(function () {
            AJS.messages.makeCloseable($container.find("div.aui-message.closeable"));
        });
    }
};

})(AJS.$);