(function ($) {

    var QuickEdit = AJS.Confluence.QuickEdit,
        container,
        editAnchor;

    function saveHandler(e) {
        //we don't have to implement anything here, it's a single page save
        AJS.log('Quick Edit page save.');
    }

    function cancelHandler() {
        //No need for implementation, it's just a link.
        AJS.log('Quick Edit cancel.');
    }

    function loadingTransition(form) {
        var contentId = Confluence.getContentId(),
            formAction = AJS.Data.get('content-type') === 'page' ? 'doeditpage' : 'doeditblogpost';
        formAction = Confluence.getContextPath() + '/pages/' + formAction + '.action?pageId=' + contentId;
        AJS.$('.ia-splitter-left').remove();
        try {
            AJS.$('#main').unwrap();
        } catch (e) {
            //this try catch is here in case a plugin puts scripts in the body which throw exceptions on unwrap.
        }
        //Don't need the editor to resize
        AJS.$("#wysiwyg").removeClass('resize');
        AJS.$('#rte').removeClass('editor-default').addClass('editor-fullheight');
        //hide stuff we don't need
        $('#watchPage').parent().parent().hide();
        container.children().remove();
        //There should be a way to remove editor-container all together but I don't have time to look into it right now.
        $('.editor-container').children().eq(0).unwrap();
        //we have to modify a lot of stuff here, would be better if the vm or soy templates set these things correctly.
        form.attr({"class": "editor aui", "action": formAction, "name": "editpageform", "id": "editpageform", "style": ""});
        container.append(form);
        container.removeClass('view').addClass('edit');
        $('body').addClass('contenteditor edit');

        return function (success) {
            AJS.populateParameters();
            var failure = function () {
                console.log('We experienced a problem click here to edit.');
                AJS.trigger('analyticsEvent', {name: 'quick-edit-failure' });
                // Redirect to "slow edit" if we fail to load the content (likely a session timeout - user needs to login)
                window.location = $('#editPageLink').attr('href');
            };
            if (success) {
                var url = Confluence.getContextPath() + "/rest/tinymce/1/content/" + contentId + ".json";
                var xhr = $.ajax({
                    url:url,
                    cache:false
                });
                xhr.success(function (data) {
                    var ed = tinymce.activeEditor;
                    ed.setContent(data.editorContent, { initialContent: true });

                    // CONFDEV-19832 - Since we are setting the editor content we also need to updated its start value
                    ed.startContent = ed.getContent({format : 'raw'});

                    // CONFDEV-13487 - Update permissions fields
                    if (data.permissions){
                        for (var perm in data.permissions) {
                            $("#" + perm).attr('value', data.permissions[perm]);
                        }
                    }
                    //Ensure the user can type immediately.
                    ed.execCommand('selectAll');
                    ed.selection.collapse(true);
                    ed.selection.getRng().startContainer.focus();
                    //Ensure first undo step doesn't blow away the content we just set.
                    ed.undoManager.clear();
                    ed.undoManager.add();
                    $('#originalVersion').val(data.pageVersion);
                    // Update token in case of session timeout and user is using "remember me"
                    AJS.Meta.set('atl-token', data.atlToken);
                    AJS.$('input[name="atl_token"]').val(data.atlToken);

                    // Add history item for main editor so pressing back
                    // worked like it used to.
                    if (supportsPushState) {
                        var editLink = $("#editPageLink").attr("href");

                        if (editLink != window.location.pathname + window.location.search) {
                            history.pushState({ quickEdit: true }, "", editLink);
                        }
                    } else {
                        window.location.hash = "editor";
                    }
                    statePushed = true;

					AJS.trigger('analyticsEvent', {name: 'quick-edit-success' });
                });
                xhr.error(failure);
            } else {
                failure();
            }
        };
    }

    var QuickPageEdit = {
        /**
         * Handle the triggering of edit
         *
         * @param e the event triggering the activation
         */
        activateEventHandler:function (e) {
            if (e.metaKey || e.shiftKey || e.ctrlKey || e.altKey || e.which == 2 || e.which == 3) {
                return;
            }
            e.preventDefault();
            QuickEdit.disableHandlers();
            var fallbackUrl = editAnchor.attr("href");
            AJS.Confluence.QuickEdit.activateEditor(container, saveHandler, cancelHandler, loadingTransition, fallbackUrl);
        },

        enable:function () {
            //don't enable/disable outside of default theme note that we can't check this until DOM ready which happens after the dark feature checks below.
            if ($('body').is('.theme-default')) {
                AJS.log("QuickPageEdit enabled");
                if (!editAnchor || editAnchor.length === 0) {
                    //first enable
                    editAnchor = $("#editPageLink");
                    editAnchor.removeClass('full-load'); // prevent default shortcut from operating
                    container = $('#content');
                }
                AJS.bind("initialize.keyboardshortcuts", function () {
                    //This is here in case keyboard shortcuts are initialized after dom ready.
                    AJS.whenIType("e").moveToAndClick(editAnchor);
                });
                editAnchor.one('click', QuickPageEdit.activateEventHandler);
            }
        },

        activateEditor: function() {
            AJS.Confluence.QuickEdit.activateEditor(container, saveHandler, cancelHandler, loadingTransition, "");
        },

        disable:function () {
            //don't enable/disable outside of default theme
            if ($('body').is('.theme-default')) {
                AJS.log("QuickPageEdit disabled.");
                editAnchor.unbind();
            }
        }
    };

    if (!($.browser.msie && +$.browser.version < 9)) {
        QuickEdit.register(QuickPageEdit);

        // Does this browser support `history.pushState` ?
        var supportsPushState = !!window.history.pushState;

        // Has a pushed state occurred? This is necessary
        // as Chrome and Firefox treat the `popstate`
        // event differently.
        var statePushed = false;

        // Handle quick edit history
        // Activating quick edit will now create a
        // history item. It falls back to hash if
        // `history.pushState` isn't supported.
        var initialUrl = location.href;

        var handlePageStateChange = function() {
            if ((supportsPushState && history.state && history.state.quickEdit)
                || window.location.hash == "#editor") {
                QuickPageEdit.activateEditor();
                statePushed = true;
            } else if (statePushed && location.href == initialUrl) {
                // I don't use reload() because that will
                // make the browser confirmation tell the
                // user they are reloading, which isn't
                // what they're expecting.
                window.location = window.location;
            }

        };

        $(window).bind(supportsPushState ? "popstate" : "hashchange", handlePageStateChange);

        // IE9 freaks out and doesn't display the editor
        // if you attempt to display it now.
        setTimeout(function() {
            ! supportsPushState && handlePageStateChange();
        }, 0);

    }
})(AJS.$);
