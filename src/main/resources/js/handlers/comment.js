(function($) {
    /**
     * Common QuickComment code, for actual handler implementations see the comment/ directory
     */
    $(function() {
        // Customise the initialisation of AppLinks so that it won't start until the editor is initialised.
        AJS.AppLinksInitialisationBinder = function(f) {
            AJS.bind("init.rte", f);
        };
    });

    var QuickEdit = AJS.Confluence.QuickEdit;
    QuickEdit.QuickComment = {
        /**
         * A success handler that will reload the page focused on the new comment.
         */
        reloadPageSaveCommentHandler: function(data) {
            var baseUrl = getBaseUrl();
            baseUrl.addQueryParam("focusedCommentId", data.id);

            var reloadUrl = baseUrl.toString();
            window.location.href = baseUrl.toString() + "#comment-" + data.id;
        },

        cancelHandler: function() {
            AJS.Rte.Content.editorResetContentChanged();
            window.location.reload();
        },
        /**
         * Most templates require a commenter parameter. This function creates it.
         * 
         * @param $userLogoImg an img representing a user log to create commenter details from
         */
        createCommenterParam: function($userLogoImg) {
            var userLogoSrc = $userLogoImg.attr("src");

            var userName = AJS.Meta.get("remote-user");
            if (userName != null && userName.length == 0) {
                userName = null;
            }
            
            return {
                "userName": userName,
                displayName: AJS.Meta.get("user-display-name"),
                profilePicture: {
                    isDefault: $userLogoImg.hasClass("defaultLogo"),
                    path: userLogoSrc
                }
            };
        },
        
        /**
         * Create a save handler which is a function taking a single event that is to be called
         * when the save operation is activated on the editor.
         * 
         * @param successHandler the function to be called if save is successful. Takes a single argument which is the 
         * data returned from the save. 
         * @param errorHandler a function taking a single message parameter which is called if the save fails.
         * @return a function taking an event parameter which is suitable for use as a save handler for the editor
         */
        createSaveHandler: function(successHandler, errorHandler) {
            var quickComment = this;
            return function(event) {
                if (!Confluence.Editor.UI.toggleSavebarBusy(true)) {
                    AJS.log("QuickComment: subsequent save operation attempted but ignored.");
                    return;
                }
                
                if (AJS.Rte.Content.isEmpty()) {
                    AJS.Confluence.EditorNotification.notify("warning", AJS.I18n.getText("content.empty"), 8);
                    return;                
                } 

                // TODO add better progress indication rather than just changing save button to "saving"

                // TODO Not required because we aren't dynamically adding comments yet
                // Learn about the users current profile picture from their avatar already rendered beside the editor
                // var commenter = quickComment.createCommenterParam(AJS.Confluence.EditorLoader.getEditorForm().closest(".quick-customcontent-container").find(".userLogo"));

                // Check for a parent comment. 
                var parentCommentId = 0;
                var $form = AJS.Confluence.EditorLoader.getEditorForm();
                if ($form.is("form")) {
                    var action = $form.attr("action");
                    var match = action.match(/parentId=(\d+)/);
                    if (match && match.length > 1) {
                        parentCommentId = parseInt(match[1]);
                    }                    
                }
                
                var $watchPage = $("#watchPage", $form);
                var watch = false;
                if ($watchPage.length) {
                    watch = $watchPage[0].checked;
                }

                // captcha
                var $captchaIdInput = AJS.$('input[name="captchaId"]', $form);
                var captchaId = null;
                if ($captchaIdInput.length) {
                    captchaId = $captchaIdInput.val();
                }
                var $captchaResponseInput = AJS.$('input[name="captchaResponse"]', $form);
                var captchaResponse = null;
                if ($captchaResponseInput.length) {
                    captchaResponse = $captchaResponseInput.val();
                }
                var captcha = {
                    id: captchaId,
                    response: captchaResponse
                };
                
                var changeCaptchaErrorHandler = function(message) {
                    errorHandler(message);
                    var $img = $("img.captcha-image", $form);
                    if ($img.length) {
                        // update captcha with new id
                        var captchaId = +Math.random();
                        $img[0].src = AJS.contextPath() + "/jcaptcha?id=" + captchaId;
                        $captchaIdInput.val(captchaId);
                        $captchaResponseInput.val("");
                    }
                };
                
                Confluence.Editor.CommentManager.saveComment(Confluence.getContentId(), parentCommentId, AJS.Rte.Content.getHtml(),
                        watch, captcha, successHandler, changeCaptchaErrorHandler);
            };
        },
        
        /*
         * Provide feedback to the user that their click has done something (has caused the editor to begin 
         * loading) and set up a timer to show a message if loading is taking too long.
         * 
         * @return a zero argument function that can be used to cancel the 'placeholder' timer. So
         * if activation completes quickly enough you can use this function to cancel the display of
         * the editor placeholder
         */
        loadingTransition: function($form, $loadingContainer, $spacer) {
            // CONFDEV-10526 - Change to 'comment' as it is by default page or blog
            AJS.Meta.set('content-type', 'comment');
            AJS.Meta.set('draft-type', '');
            AJS.Meta.set('save-drafts', false);

            // make sure AJS.params and AJS.Meta share the same value (until CONFDEV-15654 is resolved)
            AJS.params.contentType = 'comment';
            AJS.params.draftType = '';
            AJS.params.saveDrafts = false;

            // Update other values accordingly
            AJS.Meta.set('use-inline-tasks', 'false');

            $spacer.show(); // show the space to ensure the page is long enough to us to scroll to where there editor will be.
            $form.hide();
            $loadingContainer.show();
            
            // if loading is too slow then we will switch to showing a message
            var loadingTimeoutId = setTimeout(function() {
                $(".quick-customcontent-loading-message", $loadingContainer).show();
            }, 400);        
                    
            return function() {
                // CONFDEV-10529 Make sure edit is loaded before trying to manipulate it's DOM.
                $('#rte-button-save-draft').parent().remove();
                $('#editor-precursor').children().eq(0).hide();
                $('#pagelayout-menu').parent().hide();
                $('#page-layout-2-group').hide();
                $('#rte-button-tasklist').remove();
                $('#rte-insert-tasklist').parent().remove();
                var saveBar = $('#rte-savebar');
                saveBar.find('.toolbar-split-left').hide();
                //ensures that the row containing draft status & hints doesn't take up space
                saveBar.find('#draft-status').parent().parent().hide();
                saveBar.find('#notifyWatchers').parent().hide();
                saveBar.find('#versionComment').parent().hide();

                // Required for chrome as it will add 10px padding if this is not hidden when effectively empty
                var minorEdit = saveBar.find('#toolbar-group-minor-edit');
                var minorEditChildren = minorEdit.children();
                // Can't use :hidden selector in jQuery as the editor is not visible yet.
                function isHidden() {
                    return $(this).css('display') === 'none';
                }
                if(minorEditChildren.length === minorEditChildren.filter(isHidden).length) {
                    minorEdit.hide();
                }

                // we will use AJAX not form submission for comments.
                Confluence.Editor.removeAllSubmitHandlers();
                Confluence.Editor.addSubmitHandler(function(e) {
                    e.preventDefault();
                    return false;
                });
                clearTimeout(loadingTimeoutId);

                // The comment editor should always use compact mode, so enable compact mode's CSS.
                $("head").append(AJS.Meta.get("editor.loader.comment.resources"));

            };
        },
        
        saveCommentErrorHandler: function(message) {
            Confluence.Editor.UI.toggleSavebarBusy(false);
            // recognise some common error conditions
            var displayMessage = AJS.I18n.getText("quick.comment.saving.error.message") + " " + message;

            if (message && message.search(/captcha/i) != -1) {
                displayMessage = AJS.I18n.getText("captcha.response.failed");
            } 
            
            AJS.Confluence.EditorNotification.notify("error", displayMessage, 30);
        }
    };

    /**
     * Return an object representing the base URL.
     */
    var getBaseUrl = function() {
        // stripping ':' from protocol and '/' from pathname to handle cross browser inconsistency
        var baseUrl = window.location.protocol.replace(/:$/,"") + "://" + window.location.host + "/" +  window.location.pathname.replace(/^\//,"");

        var search = window.location.search.replace(/^\?/,""); // drop the leading '?'
        search = search.replace(/\&?focusedCommentId=\d+/,"");
        search = search.replace(/^\&/,"");

        return {
            url : baseUrl,

            search: search,

            addQueryParam : function(name, value) {
                if (!this.search) {
                    this.search = name + "=" + value;
                } else {
                    this.search = this.search + "&" + name + "=" + value;
                }
            },

            toString: function() {
                return this.url + "?" + this.search;
            }
        };
    }
})(AJS.$);
