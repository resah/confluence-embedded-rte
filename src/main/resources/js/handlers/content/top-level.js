(function($){
    var QuickEdit = AJS.Confluence.QuickEdit;
    var QuickContent = QuickEdit.QuickContent;

    // we override 'm' rather than change it in the keyboard-shortcuts plugin. This way if the quick-comment
    // plugin is disabled we still have the original behaviour.
    AJS.bind("initialize.keyboardshortcuts", function() {
        AJS.whenIType("m").moveToAndClick(".quick-customcontent-prompt");
    });

    /**
     * topLevelHandler for use by QuickEdit
     */
    var topLevelHandler = {

        /**
         * Called to handle the triggering of a top level comment editor.
         * This is expected to called in the context where 'this' is the activated element.
         *
         * @param e the event triggering the activation
         */
        activateEventHandler: function(e) {
            QuickEdit.disableHandlers();

            var $container = $(this).closest(".quick-customcontent-container");

            var saveHandler = QuickContent.createSaveHandler(QuickContent.reloadPageSaveCommentHandler, QuickContent.saveCommentErrorHandler);

            var fallbackUrl = $("#add-comment-rte").attr("href");
            return QuickEdit.activateEditor($container, saveHandler, QuickContent.cancelHandler, topLevelHandler.loadingTransition, fallbackUrl);
        },

        enable: function() {
            $("#customcontent-section").delegate(".quick-customcontent-prompt", "click", topLevelHandler.activateEventHandler);
            $("#add-comment-rte").removeClass("full-load"); //Ensure that the original shortcut isn't also activated.
        },

        /**
         * Ensure all top level comment place holders are disable and remove all handlers.
         */
        disable: function() {
            $("#customcontent-section").undelegate(".quick-customcontent-prompt", "click");

            /*
             * Shortcuts are added and removed as the editor is activated and deactivated so
             * bind to this event so we keey the 'm' shortcut for ourselves.
             */
            AJS.bind("add-bindings.keyboardshortcuts", function() {
                AJS.whenIType("m").moveToAndClick(".quick-customcontent-prompt");
            });
        },

        /**
         * Ensure the entirety of the comment editor will be in view. This relies on the
         * fact that the 'add comment' editor is always at the bottom of the page. If it sat
         * higher on the page this method would always result in the comment editor being
         * scrolled to the top of the page.
         */
        loadingTransition: function($form, $loadingContainer, $spacer) {
            var cancelLoadingMessage = QuickContent.loadingTransition($form, $loadingContainer, $spacer);
            $spacer[0].scrollIntoView();
            return cancelLoadingMessage;
        }
    };

    QuickEdit.register(topLevelHandler);
})(AJS.$);