(function($){
    var QuickEdit = AJS.Confluence.QuickEdit;
    var QuickComment = QuickEdit.QuickComment;

    /**
     * replyHandler for use by QuickEdit
     */
    var replyHandler = {

        /**
         * Will display the editor structure when reply is activated. Expects 'this' to be the DOM element
         * that was activated.
         *
         * @param e the event triggering the activation
         */
        activateEventHandler: function(e) {
            QuickEdit.disableHandlers();

            var $thread = $(this).closest(".comment-thread");
            var $comment = $(this).closest("div.comment");

            var $placeHolderLogo = $(".quick-customcontent-container img.userLogo");
            var commenter = QuickComment.createCommenterParam($placeHolderLogo);

            var match = $comment.attr("id").match(/comment-(\d+)/);
            var parentCommentId = 0;
            if (match) {
                parentCommentId = parseInt(match[1]);
            } else {
                AJS.log("replyHandler: activateEventHandler - could not extract a parent comment Id from the comment id " + $comment.attr("id"));
            }

            var params = {
                "contentId": Confluence.getContentId(),
                "parentCommentId": parentCommentId,
                "commenter": commenter,
                "context": {
                    "contextPath": AJS.Meta.get("context-path"),
                    "staticResourceUrlPrefix": AJS.Meta.get("static-resource-url-prefix")
                }
            };

            //since we're creating a new quick-comment-form from template we should ensure the old one isn't found by selectors.
            $(".quick-customcontent-form").removeClass("quick-customcontent-form");
            var template = Confluence.Templates.Comments.displayReplyEditorLoadingContainer(params);
            $comment.after(template);

            // Confluence.Editor.getCurrentForm requires this to match the name of the form
            // (which is important for the binding of the submit handler)
            var $container = $(".quick-customcontent-container", $thread);

            var formName = $("form", $container).attr("name");
            AJS.Meta.set("form-name", formName);

            var saveHandler = QuickComment.createSaveHandler(QuickComment.reloadPageSaveCommentHandler, QuickComment.saveCommentErrorHandler);

            var fallbackUrl = $("#reply-comment-" + parentCommentId).attr("href");

            QuickEdit.activateEditor($container, saveHandler, QuickComment.cancelHandler, replyHandler.loadingTransition, fallbackUrl);
            e.stopPropagation();
            return false;
        },

        enable: function() {
            $("#customcontent-section").delegate(".action-reply-comment", "click", replyHandler.activateEventHandler);
        },

        disable: function() {
            $("#customcontent-section").undelegate(".action-reply-comment", "click");
        },

        /**
         * Ensure the entirety of the comment editor will be in view.
         * Only scroll the page if it is necessary to fit the comment editor in and attempt to
         * scroll only enough to keep the editor in view.
         */
        loadingTransition: function($form, $loadingContainer, $spacer) {
            var cancelLoadingMessage = QuickComment.loadingTransition($form, $loadingContainer, $spacer);
            var $container = $loadingContainer.closest(".quick-customcontent-container");
            if (!scrollWindowToElement($container)) {
                // try and scroll the closest overflow set parent (e.g. Documentation Theme)
                scrollOverflowContainerToElement($container);
            }

            return cancelLoadingMessage;
        }
    };
    QuickEdit.register(replyHandler);


    /*****************
     * Utility methods
     ********/

    /**
     * Find the closest parent with the CSS property overflow set to either 'scroll' or 'auto' and
     * scroll this to show the element.
     *
     * @return true if successfully found a parent to scroll.
     */
    function scrollOverflowContainerToElement($element) {
        var $parent = null;

        $element.parents().each(function(index) {
            var overflow = $(this).css("overflow");
            if (overflow == "auto" || overflow == "scroll") {
                $parent = $(this);
                return false;
            }
        });

        if (!$parent) {
            return false;
        }

        var height = $parent.height();

        var extra = 40; // the calculation seems to be off by 40 pixels - I don't know why (perhaps padding on $element?)
        var elementVerticalPosition = $element.offset().top;
        var elementHeight = $element.height();
        var differential = height - (elementVerticalPosition + elementHeight + extra);

        if (differential < 0) {
            $parent[0].scrollTop = $parent[0].scrollTop - differential;
        }

        return true;
    }

    /**
     * The default theme has scrollbars on the window and therefore this
     * method can make sure the supplied element is visible. However other themes don't scroll the
     * window so this method will return true if it successfully scrolls and false if it is unable
     * to move the window.
     */
    function scrollWindowToElement($element) {
        function getScrollY() {
            if ('pageYOffset' in window) {  // all browsers, except IE before version 9
                return window.pageYOffset;
            } else { // Internet Explorer before version 9
                // we ignore zoom factor which was only an issue before IE8
                return document.documentElement.scrollTop;
            }
        }

        var scrollY = getScrollY();

        var windowHeight;
        if (typeof(window.innerWidth) == 'number') {
            windowHeight = window.innerHeight;
        } else if (document.documentElement && document.documentElement.clientWidth) {
            // IE 6+ in 'standards compliant mode'
            windowHeight = document.documentElement.clientHeight;
        } else {
            // something old and creaky - just try to make sure the element will be visible and return true so we consider this successful
            $element[0].scrollIntoView(false);
            return true;
        }

        var elementVerticalPosition = $element.offset().top;
        var elementHeight = $element.height();

        var extra = 40; // the calculation seems to be off by 40 pixels - I don't know why (perhaps padding on $element?)

        if ((elementVerticalPosition + elementHeight + extra) > scrollY + windowHeight) {
            $element[0].scrollIntoView(false); // align to the bottom of the viewport
            window.scrollBy(0, extra);
            return scrollY != getScrollY(); // did we successfully scroll the window?
        } else {
            // no scrolling was necessary
            return true;
        }
    }
})(AJS.$);